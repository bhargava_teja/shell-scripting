#!/bin/bash

# Display 'Hello'
echo 'Hello'

#Assign a value to a variable
WORD='script'

#Display that value
echo "$WORD"

#Demonstrate that single quotes cause variables to not get expanded
echo '$WORD'

#Combine variables with hard coded text
echo "This is a shell $WORD"

echo "This is a shell ${WORD}"

echo "${WORD}ing is fun"

#Create a new variable
ENDING='ed'

#Combine 2 varibles
echo "This is ${WORD}${ENDING}."


#Change the value stored in ENDING
ENDING='ing'

#This is changed one

echo "${WORD}${ENDING} is fun"

ENDING='s'
echo "You are going to write many ${WORD}${ENDING}" in this class
