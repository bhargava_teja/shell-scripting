#!/bin/bash

if [[ "${UID}" -ne 0 ]]
then
	echo "Please run with root privileges"
	exit 1
fi
