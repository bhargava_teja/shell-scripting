#!/bin/bash

#This script will generate random password
PASSWORD="${RANDOM}"
echo ${PASSWORD}

#Three random numbers together
PASSWORD="${RANDOM}${RANDOM}${RANDOM}"
echo "$PASSWORD"

# Use the current date/time as basis for password

PASSWORD=$(date +%s)
echo "${PASSWORD}"

# Use nanoseconds as password

PASSWORD=$(date +%s%N)
echo "${PASSWORD}"

# Use shasum as password
PASSWORD=$(date +%s%N | sha256sum | head -c32)
echo "${PASSWORD}"

# Even better Password
PASSWORD="$(date +%s%N${RANDOM}${RANDOM} | sha512sum | head -c48)"
echo "${PASSWORD}"

#Append a special character
SPECIAL_CHARACTER=$(echo '!@#$%^&*()_+' | fold -w1 | shuf | head -c1)
echo "${PASSWORD}${RANDOM}${SPECIAL_CHARACTER}"
