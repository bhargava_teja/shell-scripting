#!/bin/bash

# Creating the local users

# Ensuring that script is executed with root privileges

if [[ "${UID}" -ne 0 ]]
then
	echo "You have to be root to execute this script" >&2
	exit 1
fi

# If the user doesn't supply atleast one argument, then give them help
if [[ "${#}" -lt 1 ]]
then
	echo "Usage: ${0} USER_NAME [COMMENT]..." >&2
	exit 1
fi

# The first parameter is username
USER_NAME="${1}"

# The rest of the parameters are for the account comments
shift
COMMENT="${@}"

# Generate a password
PASSWORD=$(date +%s%N | sha256sum | head -c48)

# Create the user accounts

useradd -c "${COMMENT}" -m ${USER_NAME} &> /dev/null

# Check to see if the useradd command succeeded
# We dont want to tell the user that an account is created when it hasn't been

if [[ "${?}" -ne 0 ]]
then
	echo 'The account cannot be created' >&2
	exit 1
fi

# set the password
echo ${PASSWORD} | passwd --stdin ${USER_NAME} &> /dev/null

# Check to see if passwd command succeeded
if [[ "${?}" -ne 0 ]]
then
	echo 'The password for the account could not be set' >&2
	exit 1
fi

# Force password change

passwd -e ${USER_NAME} &> /dev/null

# Display the username and password


echo 'Username: '  "${USER_NAME}"

echo 'Password: ' "${PASSWORD}"

echo 'host: ' "${HOSTNAME}"

exit 0
